(**Main types on maps and feature on moves*)


type pos = int * int
type 'a grid = 'a array array

(** Elements of the map *)
type elt = ICE of int| WATER | PENGUIN | NOSTOP | TELEPORT of (pos * bool)

(** Represents a direction *)
type dir = E | NE | SE | W | SW | NW

(** Represents the different types of solo challenges*)
type challenge = MAX_FISH of int
		|MIN_FISH of int
		|MIN_TELEPORT of int
		|MIN_MOVE of int
			       

(** Represents a move, i.e. a dir. and a nb. of block to cross. *)
type move = dir * int
		    
(** List of all the directions.*)
val all_directions : dir list

(** Exception raised when we ask an invalid int to move computation. *)
exception InvalidMoveNumber


(** Fonctions used to translate directions into integer and back*)
val dir_of_int : int -> dir
val int_of_dir : dir -> int

(** character grid printing *)
val pp_grid : Format.formatter -> char grid -> unit

(**Functions of movement *)

(** Movement of one case, without considering the nature of the cases*)
val move : pos -> dir -> pos

(**Movement on the grid of any length*)
val move_n : elt grid ->pos -> move -> pos


(**Functions which determine the validity of a move*)
val legal_move_n : elt grid -> pos  -> move -> (bool * pos * int)

val special_legal_move_n : elt grid -> pos  -> move -> (bool * pos)

